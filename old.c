#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#define SUNS_M126_MODELID  126
#define SUNS_M126_LEN_F    10
#define SUNS_M126_LEN_R    54

/* Address offsets of fixed block registers */
#define S_126_ActCrv       2
#define S_126_ModEna       3
#define S_126_WinTms       4
#define S_126_RvrtTms      5
#define S_126_RmpTms       6
#define S_126_NCrv         7
#define S_126_NPt          8
#define S_126_V_SF         9
#define S_126_DeptRef_SF   10
#define S_126_RmpIncDec_SF 11

/* Block offsets of repeating block registers */
#define S_126_REPEAT_ActPt     0
#define S_126_REPEAT_DeptRef   1
#define S_126_REPEAT_V1        2
#define S_126_REPEAT_VAr1      3
#define S_126_REPEAT_V2        4
#define S_126_REPEAT_VAr2      5
#define S_126_REPEAT_V3        6
#define S_126_REPEAT_VAr3      7
#define S_126_REPEAT_V4        8
#define S_126_REPEAT_VAr4      9
#define S_126_REPEAT_V5        10
#define S_126_REPEAT_VAr5      11
#define S_126_REPEAT_V6        12
#define S_126_REPEAT_VAr6      13
#define S_126_REPEAT_V7        14
#define S_126_REPEAT_VAr7      15
#define S_126_REPEAT_V8        16
#define S_126_REPEAT_VAr8      17
#define S_126_REPEAT_V9        18
#define S_126_REPEAT_VAr9      19
#define S_126_REPEAT_V10       20
#define S_126_REPEAT_VAr10     21
#define S_126_REPEAT_V11       22
#define S_126_REPEAT_VAr11     23
#define S_126_REPEAT_V12       24
#define S_126_REPEAT_VAr12     25
#define S_126_REPEAT_V13       26
#define S_126_REPEAT_VAr13     27
#define S_126_REPEAT_V14       28
#define S_126_REPEAT_VAr14     29
#define S_126_REPEAT_V15       30
#define S_126_REPEAT_VAr15     31
#define S_126_REPEAT_V16       32
#define S_126_REPEAT_VAr16     33
#define S_126_REPEAT_V17       34
#define S_126_REPEAT_VAr17     35
#define S_126_REPEAT_V18       36
#define S_126_REPEAT_VAr18     37
#define S_126_REPEAT_V19       38
#define S_126_REPEAT_VAr19     39
#define S_126_REPEAT_V20       40
#define S_126_REPEAT_VAr20     41
#define S_126_REPEAT_CrvNam    42
#define S_126_REPEAT_RmpTms    50
#define S_126_REPEAT_RmpDecTmm 51
#define S_126_REPEAT_RmpIncTmm 52
#define S_126_REPEAT_ReadOnly  53

/* Symbol definitions */
#define S_126_ModEna_ENABLED            0
#define S_126_REPEAT_DeptRef_WMax       1
#define S_126_REPEAT_DeptRef_VArMax     2
#define S_126_REPEAT_DeptRef_VArAval    3
#define S_126_REPEAT_ReadOnly_READWRITE 0
#define S_126_REPEAT_ReadOnly_READONLY  1

/* other definitions */
#define SUNS_HEADER_LEN     2
#define AINV_NPOINT         10
#define AINV_NCURVE         4

#define TRUE true
#define FALSE false

volatile struct {
	union {
		int16_t w;

		struct {
			unsigned UseRelaxedSAChecks : 1;
			unsigned : 15;
		};
	} ControlBits;
} SPT;

volatile struct {
	struct {
		struct {
			struct {
				int16_t Val, Out;
			} Pt[AINV_NPOINT];

			int16_t Npt;
			int16_t DeptRef;
		} Curve[AINV_NCURVE];

		int16_t Cact;
		int16_t PropCact;
		bool Update;
	} VoltQ;
} Ainv;

typedef int16_t dataType;

struct datatype_holder_t {
	int16_t datatype;
} suns_meta_m126_f[SUNS_M126_LEN_F];
struct datatype_holder_t suns_meta_m126_r[SUNS_M126_LEN_R];

int16_t getUnimpleValFromDatatype( int16_t datatype ) {
	return 9000;
}

int16_t suns_read_m126_volt_var(uint16_t address_offset, uint16_t length, int16_t* buffer) {
    uint16_t current_register;
    uint16_t i;
    uint16_t num_read = 0;
    int16_t value = 0;

    if (address_offset < S_126_ActCrv){
        // register is within model header, which is supposed be handled by library
        return num_read;
    }

    for (i = 0; i < length; i++) {
        bool unimplemented = false;
        current_register = address_offset + i;
        uint16_t rep_index, rep_offset = 0;

        // check if register is in repeating block
        bool in_repeat_block = (current_register >= SUNS_M126_LEN_F + SUNS_HEADER_LEN);
        if (in_repeat_block) {
            rep_index = ( current_register - SUNS_M126_LEN_F - SUNS_HEADER_LEN ) / SUNS_M126_LEN_R;
            rep_offset = ( current_register - SUNS_M126_LEN_F - SUNS_HEADER_LEN ) % SUNS_M126_LEN_R;
            if ( rep_index < AINV_NCURVE ) {
                switch ( rep_offset ) {
                    case S_126_REPEAT_ActPt:
                        value = Ainv.VoltQ.Curve[rep_index].Npt;
                        break;
                    case S_126_REPEAT_DeptRef:
                        value = Ainv.VoltQ.Curve[rep_index].DeptRef;
                        break;
                    case S_126_REPEAT_ReadOnly:
                        if ( ( rep_index + 1 ) == Ainv.VoltQ.Cact ) {
                            // this curve is read only
                            value = S_126_REPEAT_ReadOnly_READONLY;
                        } else {
                            // the curve is read-write
                            value = S_126_REPEAT_ReadOnly_READWRITE;
                        }
                        break;
                    default:
                    {
                        // for readability: (I'm pretty sure the compiler will optimize out the extra variables)
                        int16_t array_index = ( rep_offset - S_126_REPEAT_V1 ) >> 1;

                        // if X_or_Y is 0, this register corresponds with the X coordinate, if it's 1 it corresponds with the Y coordinate
                        int16_t X_or_Y = ( rep_offset - S_126_REPEAT_V1 ) % 2;

                        // check if the register corresponds to a valid curve point
                        int16_t npoint;
                        if ( SPT.ControlBits.UseRelaxedSAChecks ) npoint = AINV_NPOINT;
                        else npoint = 4;
                        if ( ( array_index >= 0 ) && ( array_index < npoint ) ) {
                            // pass through the point value
                            if ( X_or_Y == 0 )
                                value = Ainv.VoltQ.Curve[rep_index].Pt[array_index].Val;
                            else
                                value = Ainv.VoltQ.Curve[rep_index].Pt[array_index].Out;
                        } else {
                            unimplemented = true;
                        }
                        break;
                    }
                }
            } else {
                // curve index is too high, so return the appropriate unimplemented value
                unimplemented = true;
            }

        } else  // otherwise check fixed block
        switch (current_register) {
            // write requested point data to the "value" variable
            case S_126_ActCrv:
                value = Ainv.VoltQ.Cact;
                break;
            case S_126_ModEna:
                value = (Ainv.VoltQ.Cact != 0);
                break;
            case S_126_NCrv:
                value = AINV_NCURVE;
                break;
            case S_126_NPt:
                value = AINV_NPOINT;
                break;
            case S_126_V_SF:
                value = -1;
                break;
            case S_126_DeptRef_SF:
                value = -1;
                break;
            case S_126_RmpIncDec_SF:
                value = 0;
                break;
            default:
                unimplemented = true;
                break;
        }

        if ( unimplemented && current_register >= SUNS_HEADER_LEN ) {
            // use the library to get the correct unimplemented value for this register
            dataType datatype;
            if ( in_repeat_block ) {
                datatype = suns_meta_m126_r[rep_offset].datatype;
            } else
            {
                uint16_t block_offset = current_register - SUNS_HEADER_LEN;  // convert address offset to block offset
                datatype = suns_meta_m126_f[block_offset].datatype;
            }
            value = getUnimpleValFromDatatype( datatype );
        }

        buffer[i] = value;
        num_read++;
    }

    return num_read;
}

int16_t suns_write_m126_volt_var(uint16_t address_offset, uint16_t length, int16_t* buffer) {
    uint16_t num_written = 0;
    uint16_t i;

    if (address_offset < S_126_ActCrv){
        return num_written;
    }

    for (i = 0; i < length; i++) {
        uint16_t current_register = address_offset + i;
        int16_t value = buffer[i];
        if ( current_register < ( SUNS_M126_LEN_F + SUNS_HEADER_LEN ) ) // register is in the fixed block
        {
            switch ( current_register ) {
                case S_126_ActCrv:
                    if ( ( value >= 0 ) && ( value <= AINV_NCURVE ) )
                        Ainv.VoltQ.PropCact = value;
                    num_written++;
                    break;
                case S_126_ModEna:
                    if ( value & ( 1 << S_126_ModEna_ENABLED ) ) {
                        Ainv.VoltQ.Update = TRUE;
                    }
                    num_written++;
                    break;
                default:
                    break;
            }
        } else { // register is in the repeating block
            int16_t rep_index = ( current_register - SUNS_M126_LEN_F - SUNS_HEADER_LEN ) / SUNS_M126_LEN_R;
            int16_t rep_offset = ( current_register - SUNS_M126_LEN_F - SUNS_HEADER_LEN ) % SUNS_M126_LEN_R;

            if ( ( rep_index < AINV_NCURVE ) && ( ( rep_index + 1 ) != Ainv.VoltQ.Cact ) ) {
                switch ( rep_offset ) {
                    case S_126_REPEAT_ActPt:
                        if ( SPT.ControlBits.UseRelaxedSAChecks ) {     //# points fixed at 4 if not relaxed
                            if ( value <= AINV_NPOINT )
                                Ainv.VoltQ.Curve[rep_index].Npt = value;
                        }
                        num_written++;
                        break;
                    case S_126_REPEAT_DeptRef:
                        if ( value < 1 )
                            value = 1;
                        else if ( value > 2 )
                            value = 2;
                        Ainv.VoltQ.Curve[rep_index].DeptRef = value;
                        num_written++;
                        break;
                    default:
                    {
                        // for readability: (I'm pretty sure the compiler will optimize out the extra variables)
                        int16_t array_index = ( rep_offset - S_126_REPEAT_V1 ) >> 1;

                        // if X_or_Y is 0, this register corresponds with the X coordinate, if it's 1 it corresponds with the Y coordinate
                        int16_t X_or_Y = ( rep_offset - S_126_REPEAT_V1 ) % 2;

                        // check if the register corresponds to a valid curve point
                        int16_t npoint;
                        if ( SPT.ControlBits.UseRelaxedSAChecks ) npoint = AINV_NPOINT;
                        else npoint = 4;
                        if ( array_index >= 0 && array_index < npoint ) {
                            // pass through the point value
                            if ( X_or_Y == 0 ) {
                                Ainv.VoltQ.Curve[rep_index].Pt[array_index].Val = value;
                                num_written++;
                            }
                            else if ( ( SPT.ControlBits.UseRelaxedSAChecks ) || ( array_index == 0 ) || ( array_index == 3 ) ) {
                                Ainv.VoltQ.Curve[rep_index].Pt[array_index].Out = value;
                                num_written++;
                            } else {
                                //restricted Y values for the middle two points are 0, not writable
                            }
                        }

                        // we don't need to do anything on attempts to write to any other registers
                        break;
                    }
                }
            }
        }
    }

    return num_written;
}

/*----------------------------------------------------------------------------*/

#include <sys/time.h>

typedef long long usec_t;

usec_t get_time() {
	struct timeval current_time; 
	if (gettimeofday(&current_time, 0) == 0) {
		return (usec_t)current_time.tv_sec * 1000000ll + (usec_t)current_time.tv_usec;
	} else {
		return -1;
	}
}

#define MODEL_LENGTH (SUNS_M126_LEN_F + SUNS_M126_LEN_R * AINV_NCURVE)
#define REPETITIONS 1000000
int main() {
	usec_t start_time;
	usec_t timespan;
	int16_t buffer[MODEL_LENGTH];

	SPT.ControlBits.UseRelaxedSAChecks = true;

	puts("old-model statistics:");

	/* Read test */
	start_time = get_time();
	for (int rep = 0; rep < REPETITIONS; ++rep) {
		suns_read_m126_volt_var(S_126_ActCrv, MODEL_LENGTH, buffer);
	}
	timespan = get_time() - start_time;
	printf("%9lld microseconds for %d whole-model reads\n", timespan, REPETITIONS);

	/* Write test */
	start_time = get_time();
	for (int rep = 0; rep < REPETITIONS; ++rep) {
		suns_write_m126_volt_var(S_126_ActCrv, MODEL_LENGTH, buffer);
	}
	timespan = get_time() - start_time;
	printf("%9lld microseconds for %d whole-model writes\n", timespan, REPETITIONS);

	return 0;
}
